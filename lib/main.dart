import 'package:flutter/material.dart';
import 'package:flutter_beginner/screens/splash_screen.dart';


//import 'screens/login.dart';

void main() {
  
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MTN App Academy',
      theme: ThemeData(
          primarySwatch: Colors.yellow
      ),
      
    home: SplashScreen(),
    );
  }
}
