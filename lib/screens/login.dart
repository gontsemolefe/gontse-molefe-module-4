import 'package:flutter/material.dart';

import 'dashboard.dart';

class Login extends StatelessWidget {
  const Login({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView(
          children: [
            Column(
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    Image.asset("assets/images/LoginBG.png")
                  ],
                ),

                Container(
                  transform: Matrix4.translationValues(0, -60, 0),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 53.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      // ignore: prefer_const_literals_to_create_immutables
                      children: [
                        const Text("Login ",
                          style: TextStyle(
                          fontFamily: "Roboto Bold",
                          color: Color(0xff000000),
                          fontSize: 35.0,)
                        ),
                        
                        // ignore: prefer_const_constructors
                        SizedBox(
                          height: 10,
                        ),

                        const Text("sign up to continue ",
                          style: TextStyle(
                          fontFamily: "Roboto Light",
                          color: Color(0xff000000),
                          fontSize: 18.0,),
                        ),

                        // ignore: prefer_const_constructors
                        SizedBox(
                          height:25.0,
                        ),

                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          obscureText: false,
                          decoration: const InputDecoration(
                            labelText: "Email",
                            border: OutlineInputBorder(),
                            prefixIcon: Icon(Icons.email),
                           // suffixIcon: Icon(Icons.remove_red_eye),
                          ),
                        ),

                        const SizedBox(
                          height: 25,
                        ),
                  
                        TextFormField(
                          keyboardType: TextInputType.visiblePassword,
                          obscureText: true,
                          decoration: const InputDecoration(
                            labelText: "Password",
                            border: OutlineInputBorder(),
                            prefixIcon: Icon(Icons.lock),
                            //suffixIcon: Icon(Icons.remove_red_eye),
                          ),
                        ),

                        const SizedBox(
                          height: 25,
                        ),

                        const Text("FORGOT YOUR PASSWORD",
                          style: TextStyle(
                          fontSize: 12,
                          ),
                        ),

                        const SizedBox(
                          height: 25,
                        ),
                  
                        Container(
                          height: 40,
                          width: double.infinity,
                          decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Color.fromARGB(255, 206, 185, 3),
                          ),

                          child: MaterialButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                builder: (context) => const Dashboard(),
                                ),
                              );
                            },

                            child: const Text(
                              "Login",
                              style: TextStyle(
                              fontSize: 20,
                              color: Colors.black,
                              ),
                            ),
                          ),
                        ),

                      ],
                      
                    ),
                  ),
                )

              ],
            ),
          ],
          


        ),
      ),
    );
  }
}