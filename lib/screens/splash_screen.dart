import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_beginner/screens/register.dart';
import 'login.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
 
  @override
  void initState() {
    super.initState();

    Timer(
        const Duration(seconds: 3),
        () => Navigator.pushReplacement(
            context, MaterialPageRoute(
            builder: (context) => const Register()
            ),
        ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color.fromARGB(255, 211, 193, 27),
              Color(0xffeb8c00),
              Color(0xffffffff)
            ],
            begin: Alignment(0.0, 0.1),
            end: Alignment.bottomCenter,
          ),
        ),
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.center,
          //mainAxisAlignment: MainAxisAlignment.values,
          children: [
            Container(
              padding: const EdgeInsets.fromLTRB(0, 130, 0, 10),
              child: Image.asset(
                "assets/images/mtn_logo.png",
                //height: 300,
                width: 200,
              ),
            ),
            const Text(
              "App Academy",
              style: TextStyle(
                fontFamily: "Roboto Bold Italic",
                color: Color(0xff000000),
                fontSize: 50.0,
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(0, 150, 0, 0),
              child: const CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              ),
            ),
            const Spacer(),
            Container(
              padding: const EdgeInsets.fromLTRB(0, 80, 0, 15),
              child: const Center(
                child: Text(
                  "POWERED BY",
                  style: TextStyle(
                    color: Color(0xff000000),
                    fontWeight: FontWeight.w800,
                    fontSize: 20.0,
                  ),
                ),
              ),
            ),
            
              Image.asset(
              'assets/images/ITv-Logo.png',
              width: 200,
              //height: 0,
            ),
          ],
        ),
      ),
    );
  }
}
